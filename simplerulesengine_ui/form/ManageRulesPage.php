<?php
/*
 * ------------------------------------------------------------------------------------
 * Created by SAN Business Consultants
 * Designed and implemented by Frank Font (ffont@sanbusinessconsultants.com)
 * In collaboration with Andrew Casertano (acasertano@sanbusinessconsultants.com)
 * Open source enhancements to this module are welcome!  Contact SAN to share updates.
 *
 * Copyright 2014 SAN Business Consultants, a Maryland USA company (sanbusinessconsultants.com)
 *  
 * Licensed under the GNU General Public License, Version 2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.gnu.org/copyleft/gpl.html
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ------------------------------------------------------------------------------------
 *  
 * This is a simple decision support engine module for Drupal.
 */


namespace simplerulesengine;

/**
 * This class returns the list of available Rules
 *
 * @author Frank Font
 */
abstract class ManageRulesPage
{

    protected $m_oSREngine      = NULL;
    protected $m_oSREContext    = NULL;
    protected $m_aURLs          = NULL;
    protected $m_aUserRights    = NULL;
    
    public function __construct($oSREngine,$aURLs,$aUserRights='VAED')
    {
        $this->m_oSREngine      = $oSREngine;
        $this->m_oSREContext    = $oSREngine->getSREContext();
        $this->m_aURLs          = $aURLs;
        $this->m_aUserRights    = $aUserRights;
    }
    
    /**
     * Get all the form contents for rendering
     * @return type renderable array
     */
    function getForm($form, &$form_state, $disabled, $myvalues, $aHtmlClassnameOverrides=NULL)
    {
        if($aHtmlClassnameOverrides == NULL)
        {
            //Set the default values.
            $aHtmlClassnameOverrides = array();
            $aHtmlClassnameOverrides['data-entry-area1'] = 'data-entry-area1';
            $aHtmlClassnameOverrides['table-container'] = 'table-container';
            $aHtmlClassnameOverrides['action-buttons'] = 'action-buttons';
            $aHtmlClassnameOverrides['action-button'] = 'action-button';
        }
        $form["data_entry_area1"] = array(
            '#prefix' => "\n<section class='{$aHtmlClassnameOverrides['data-entry-area1']}'>\n",
            '#suffix' => "\n</section>\n",
        );

        $form["data_entry_area1"]['table_container'] = array(
            '#type' => 'item', 
            '#prefix' => '<div class="'.$aHtmlClassnameOverrides['table-container'].'">',
            '#suffix' => '</div>', 
            '#tree' => TRUE,
        );

        $rows = "\n";
        global $base_url;
        $rule_tablename = $this->m_oSREContext->getRuleTablename();        
        $sSQL = "SELECT"
                . " `category_nm`, `rule_nm`, `version`, `explanation`, `summary_msg_tx`, `msg_tx`,"
                . " `req_ack_yn`, `active_yn`, `trigger_crit`, `readonly_yn`, `updated_dt` "
                . " FROM `$rule_tablename` ORDER BY rule_nm";
        $result = db_query($sSQL);
        foreach($result as $item) 
        {
            $activeyesno = ($item->active_yn == 1 ? 'Yes' : 'No');
            $readonlyyesno = ($item->readonly_yn == 1 ? 'Yes' : 'No');
            $trigger_crit = $item->trigger_crit;
            if(strlen($trigger_crit) > 40)
            {
                $trigger_crit = substr($trigger_crit, 0,40) . '...';
            }
            if(strpos($this->m_aUserRights,'V') === FALSE)
            {
                $sViewMarkup = '';
            } else {
                $sViewMarkup = '<a href="javascript:window.location.href=\''.$base_url.'/'.$this->m_aURLs['view'].'?rn='.$item->rule_nm.'\'">View</a>';
            }
            if(strpos($this->m_aUserRights,'E') === FALSE || $item->readonly_yn == 1)
            {
                $sEditMarkup = '';
            } else {
                $sEditMarkup = '<a href="javascript:window.location.href=\''.$base_url.'/'.$this->m_aURLs['edit'].'?rn='.$item->rule_nm.'\'">Edit</a>';
            }
            if(strpos($this->m_aUserRights,'D') === FALSE || $item->readonly_yn == 1)
            {
                $sDeleteMarkup = '';
            } else {
                $sDeleteMarkup = '<a href="javascript:window.location.href=\''.$base_url.'/'.$this->m_aURLs['delete'].'?rn='.$item->rule_nm.'\'">Delete</a>';
            }
            $rows   .= "\n".'<tr><td>'
                    .$item->rule_nm.'</td><td>'
                    .$item->category_nm.'</td><td>'
                    .$activeyesno.'</td><td>'
                    .$readonlyyesno.'</td><td>'
                    .$trigger_crit.'</td><td>'
                    .$item->updated_dt.'</td>'
                    .'<td>'.$sViewMarkup.'</td>'
                    .'<td>'.$sEditMarkup.'</td>'
                    .'<td>'.$sDeleteMarkup.'</td>'
                    .'</tr>';
        }

        $form["data_entry_area1"]['table_container']['ci'] = array('#type' => 'item',
                 '#markup' => '<table id="my-raptor-dialog-table" class="dataTable">'
                            . '<thead><tr><th>'.t('Rule Name').'</th><th>Category</th>'
                            . '<th>'.t('Active').'</th>'
                            . '<th>'.t('Readonly').'</th>'
                            . '<th>'.t('Trigger Criteria').'</th>'
                            . '<th>'.t('Updated').'</th>'
                            . '<th>'.t('View').'</th>'
                            . '<th>'.t('Edit').'</th>'
                            . '<th>'.t('Delete').'</th>'
                            . '</tr>'
                            . '</thead>'
                            . '<tbody>'
                            . $rows
                            .  '</tbody>'
                            . '</table>');
        
        
        $form["data_entry_area1"]['action_buttons'] = array(
             '#type' => 'item', 
             '#prefix' => '<div class="'.$aHtmlClassnameOverrides['action-buttons'].'">',
             '#suffix' => '</div>', 
             '#tree' => TRUE,
        );

        if(strpos($this->m_aUserRights,'A') !== FALSE)
        {
            $form['data_entry_area1']['action_buttons']['addrule'] = array('#type' => 'item'
                     , '#markup' => '<a href="'.$base_url.'/'.$this->m_aURLs['add'].'" >'.t('Add Rule').'</a>');
        }
       
        $form['data_entry_area1']['action_buttons']['return'] = array('#type' => 'item'
                , '#markup' => '<a href="'.$base_url.'/'.$this->m_aURLs['return'].'" >'.t('Exit').'</a>');

        return $form;
    }
}
