<?php
/*
 * ------------------------------------------------------------------------------------
 * Created by SAN Business Consultants
 * Designed and implemented by Frank Font (ffont@sanbusinessconsultants.com)
 * In collaboration with Andrew Casertano (acasertano@sanbusinessconsultants.com)
 * Open source enhancements to this module are welcome!  Contact SAN to share updates.
 *
 * Copyright 2014 SAN Business Consultants, a Maryland USA company (sanbusinessconsultants.com)
 *  
 * Licensed under the GNU General Public License, Version 2 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.gnu.org/copyleft/gpl.html
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ------------------------------------------------------------------------------------
 *  
 * This is a simple decision support engine module for Drupal.
 */


namespace simplerulesengine;

require_once('RulePageHelper.php');

/**
 * This class returns the Admin Information input content
 *
 * @author Frank Font
 */
class EditRulePage
{
    protected $m_rule_nm        = NULL;
    protected $m_oSREngine      = NULL;
    protected $m_oSREContext    = NULL;
    protected $m_aURLs          = NULL;
    protected $m_oPageHelper    = NULL;
    protected $m_sRuleclassname = NULL;
    
    function __construct($rule_nm, $oSREngine, $aURLs, $sRuleclassname=NULL)
    {
        if (!isset($rule_nm) || is_numeric($rule_nm)) {
            die("Missing or invalid rule_nm value = " . $rule_nm);
        }
        $this->m_rule_nm     = $rule_nm;
        $this->m_oSREngine = $oSREngine;
        $this->m_oSREContext = $oSREngine->getSREContext();
        $this->m_aURLs = $aURLs;
        $this->m_sRuleclassname = $sRuleclassname;
        $this->m_oPageHelper = new \simplerulesengine\RulePageHelper($oSREngine, $aURLs, $sRuleclassname);
    }
    
    /**
     * Get the values to populate the form.
     * @param type $sProtocolName the user id
     * @return type result of the queries as an array
     */
    function getFieldValues()
    {
        return $this->m_oPageHelper->getFieldValues($this->m_rule_nm);
    }
    
    /**
     * Validate the proposed values.
     * @param type $form
     * @param type $myvalues
     * @return true if no validation errors detected
     */
    function looksValid($form, $myvalues)
    {
        return $this->m_oPageHelper->looksValid($form, $myvalues, 'E');
    }
    
    /**
     * Write the values into the database.
     * @param type associative array where 'username' MUST be one of the values so we know what record to update.
     */
    function updateDatabase($form, $myvalues)
    {
        $aConsolidation = $this->m_oPageHelper->getConsolidatedExpression($myvalues);
        $sExpression = $aConsolidation['expression'];

        $tablename = $this->m_oSREContext->getRuleTablename();
        
        $updated_dt = date("Y-m-d H:i", time());
        try
        {
            if(!isset($myvalues['readonly_yn']))
            {
                $myvalues['readonly_yn'] = 0;
            }
            $nUpdated = db_update($tablename)->fields(array(
                'category_nm' => $myvalues['category_nm'],
                'version' => $myvalues['version'],
                'explanation' => $myvalues['explanation'],
                'summary_msg_tx' => $myvalues['summary_msg_tx'],
                'msg_tx' => $myvalues['msg_tx'],
                'req_ack_yn' => $myvalues['req_ack_yn'],
                'readonly_yn' => $myvalues['readonly_yn'],
                'active_yn' => $myvalues['active_yn'],
                'trigger_crit' => $sExpression,
                'updated_dt' => $updated_dt,
                ))
                    ->condition('rule_nm', $myvalues['rule_nm'],'=')
                    ->execute(); 
        } catch (\Exception $ex) {
              error_log("Failed to add rule into database!\n" . print_r($myvalues, TRUE) . '>>>'. print_r($ex, TRUE));
              drupal_set_message('Failed to save edited rule because ' . $ex->getMessage());
              return 0;
        }
        if ($nUpdated !== 1) 
        {
            
            error_log("Failed to edit user back to database!\n" . var_dump($myvalues));
            drupal_set_message('Updated ' . $nUpdated . ' records instead of 1!');
            return 0;
        }
        
        //Returns 1 if everything was okay.
        drupal_set_message('Saved update for ' . $myvalues['rule_nm']);
        return $nUpdated;
    }
    
    /**
     * @return array of all option values for the form
     */
    function getAllOptions()
    {
        return $this->m_oPageHelper->getAllOptions();
    }
    
    /**
     * Get all the form contents for rendering
     * @return type renderable array
     */
    function getForm($form, &$form_state, $disabled, $myvalues, $aHtmlClassnameOverrides=NULL)
    {
        if($aHtmlClassnameOverrides == NULL)
        {
            //Set the default values.
            $aHtmlClassnameOverrides = array();
            $aHtmlClassnameOverrides['data-entry-area1'] = 'data-entry-area1';
            $aHtmlClassnameOverrides['action-buttons'] = 'action-buttons';
            $aHtmlClassnameOverrides['action-button'] = 'action-button';
        }
        $disabled = FALSE; //They can edit the fields.
        
        $form = $this->m_oPageHelper->getForm('E',$form, $form_state, $disabled, $myvalues, $aHtmlClassnameOverrides);
        

        $rule_nm = $myvalues['rule_nm'];

        //Hidden values for key fields
        $form['hiddenthings']['rule_nm'] 
            = array('#type' => 'hidden', '#value' => $rule_nm, '#disabled' => FALSE);        
        $newversionnumber = (isset($myvalues['version']) ? $myvalues['version'] + 1 : 1);
        $form['hiddenthings']['version'] 
            = array('#type' => 'hidden', '#value' => $newversionnumber, '#disabled' => FALSE);        
        $showfieldname = 'show_rule_nm';


        //Do NOT let the user edit these values...
        $form["data_entry_area1"][$showfieldname]     = array(
            '#type' => 'textfield',
            '#title' => t('Rule Name'),
            '#value' => $rule_nm,
            '#size' => 40,
            '#maxlength' => 40,
            '#required' => TRUE,
            '#description' => t('Must be unique'),
            '#disabled' => TRUE
        );
        $newversionnumber = (isset($myvalues['version']) ? $myvalues['version'] + 1 : 1);
        $form["data_entry_area1"]['show_version']     = array(
            '#type' => 'textfield',
            '#title' => t('Version Number'),
            '#value' => $newversionnumber,
            '#size' => 4,
            '#maxlength' => 4,
            '#required' => TRUE,
            '#description' => t('Increases each time change is saved'),
            '#disabled' => TRUE
        );

        
        
        //Add the action buttons.
        $form['data_entry_area1']['action_buttons']           = array(
            '#type' => 'item',
            '#prefix' => '<div class="'.$aHtmlClassnameOverrides['action-buttons'].'">',
            '#suffix' => '</div>',
            '#tree' => TRUE
        );
        $form['data_entry_area1']['action_buttons']['create'] = array(
            '#type' => 'submit',
            '#attributes' => array(
                'class' => array($aHtmlClassnameOverrides['action-button'])
            ),
            '#value' => t('Save Rule Updates'),
            '#disabled' => FALSE
        );

        global $base_url;
        $returnURL = $base_url . '/'. $this->m_aURLs['return'];
        $form['data_entry_area1']['action_buttons']['manage'] = array('#type' => 'item'
                , '#markup' => '<a class="'.$aHtmlClassnameOverrides['action-button'].'" href="'.$returnURL.'" >'.t('Cancel').'</a>');
        
        return $form;
    }
}
